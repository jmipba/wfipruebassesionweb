/*
 * Copyright (c) 2018, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g210.hgfzergabideacomunweblib.web.controllers;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.izfe.g210.hgfzergabideacomunweblib.web.controllers.bean.SesionValue;
import net.izfe.g210.hgfzergabideacomunweblib.web.controllers.bean.SesionValues;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jmipba
 *
 */
@Controller
@RequestMapping(value = "sesion")
public class SesionController {
	
	  @RequestMapping(value = "add", method = RequestMethod.GET)
	  @ResponseBody
	  public SesionValues add(@RequestParam("nombre") final String nombre,
			  @RequestParam("valor") final String valor, final HttpServletRequest request, final HttpServletResponse response){
		  
		  HttpSession session = request.getSession();
		  session.setAttribute(nombre, valor);
		  
		  return this.getValues(session, "");
	  }
	  
	  @RequestMapping(value = "remove", method = RequestMethod.GET)
	  @ResponseBody
	  public SesionValues remove(@RequestParam("nombre") final String nombre,
			  final HttpServletRequest request,  final HttpServletResponse response){

		  HttpSession session = request.getSession();
		  session.removeAttribute(nombre);
		  
		  return this.getValues(session, "");
	  }	  

	  @RequestMapping(value="/", method = RequestMethod.GET)
	  @ResponseBody
	  public SesionValues get(final HttpServletRequest request, final HttpServletResponse response){

		  HttpSession session = request.getSession();
		  return this.getValues(session, "");
	  }	 

	  @RequestMapping(value="/get", method = RequestMethod.GET)
	  @ResponseBody
	  public HttpSession getAll(final HttpServletRequest request, final HttpServletResponse response){

		  HttpSession session = request.getSession();
		  return session;
	  }	
	  
	  @RequestMapping(value="/invalidate", method = RequestMethod.GET)
	  @ResponseBody
	  public SesionValues invalidate(final HttpServletRequest request, final HttpServletResponse response){
		  
		  HttpSession session = request.getSession(false);
		  String idOld = session.getId();
		  session.invalidate();
		  session = request.getSession();
		  session.setAttribute("k1", "v1");
		  session.setAttribute("k2", "v2");
		  session.setAttribute("k3", "v3");
		  
		  return this.getValues(session, idOld);
	  }

	  @RequestMapping(value="/create", method = RequestMethod.GET)
	  @ResponseBody
	  public SesionValues create(final HttpServletRequest request, final HttpServletResponse response){
		  
		  HttpSession session = request.getSession(true);
		  
		  return this.getValues(session, "");
	  }	  
	  
	  private SesionValues getValues(final HttpSession session, String idOld){
		  List<SesionValue> resultado = new ArrayList<>();
		  
		  Enumeration<String> attributeNames = session.getAttributeNames();
		  
		  while(attributeNames.hasMoreElements()){
			  String name = attributeNames.nextElement();
			  Object value = (String)session.getAttribute(name);			  
			  SesionValue entry = new SesionValue(name, value);
			  resultado.add(entry);
		  }
		  
		  return new SesionValues(resultado, idOld, session.getId());

	  }
	  
	  

}
