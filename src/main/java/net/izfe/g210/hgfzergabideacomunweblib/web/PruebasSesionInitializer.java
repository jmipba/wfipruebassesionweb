/*
 * Copyright (c) 2016, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g210.hgfzergabideacomunweblib.web;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import net.izfe.g240.wfiframeworkizfelib.presentacion.listeners.Log4jLocalIzfeListener;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;


public class PruebasSesionInitializer extends AbstractAnnotationConfigDispatcherServletInitializer implements
    WebApplicationInitializer {

	  @Override
	  public void onStartup(final ServletContext servletContext) throws ServletException {
	    super.onStartup(servletContext);
	    // servletContext.addFilter("FrameworkIzfeFilter", FrameworkIzfeFilter.class).addMappingForUrlPatterns(null, false,
	    // "/*");
	    servletContext.addListener(new Log4jLocalIzfeListener());
	  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/" };
  }

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[] {PruebasSesionMvc.class};
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class[] { };
  }

}
