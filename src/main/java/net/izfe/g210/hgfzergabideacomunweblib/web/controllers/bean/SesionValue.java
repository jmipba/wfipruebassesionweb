/*
 * Copyright (c) 2018, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g210.hgfzergabideacomunweblib.web.controllers.bean;

/**
 * @author jmipba
 *
 */
public class SesionValue{
	  private String name;
	  private Object value;
	  
	  public SesionValue(String name, Object value){
		  this.name = name;
		  this.value = value;
	  }

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String Object) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return this.value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	  
	  
}

