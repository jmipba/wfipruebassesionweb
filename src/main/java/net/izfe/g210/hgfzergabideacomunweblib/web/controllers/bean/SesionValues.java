/*
 * Copyright (c) 2018, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g210.hgfzergabideacomunweblib.web.controllers.bean;

import java.util.List;

/**
 * @author jmipba
 *
 */
public class SesionValues{
	  private String id;
	  private String idOld;
	  private List<SesionValue> values;
	  public SesionValues(){
		  
	  }
	  public SesionValues(List<SesionValue> values, String id){
		  this.values = values;
		  this.id = id;
	  }
	  public SesionValues(List<SesionValue> values, String idOld, String id){
		  this.values = values;
		  this.id = id;
		  this.idOld = idOld;
	  }
	/**
	 * @return the values
	 */
	public List<SesionValue> getValues() {
		return this.values;
	}
	/**
	 * @param values the values to set
	 */
	public void setValues(List<SesionValue> values) {
		this.values = values;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the idOld
	 */
	public String getIdOld() {
		return this.idOld;
	}
	/**
	 * @param idOld the idOld to set
	 */
	public void setIdOld(String idOld) {
		this.idOld = idOld;
	}

	
	  
}
